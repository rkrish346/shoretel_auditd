# Puppet Manifest to run log archive script

class shoretel_auditd::audit_service (

  $enable_audit_service = true,
  $auditd_conf_log_file = '/var/log/audit/audit.log',

){

  if ( $enable_audit_service) {
      package { 'audit':
        ensure => present,
        notify => Service['auditd']
      }

      package { 'audit-libs':
        ensure  => present,
        require => Package['audit'],
        notify  => Service['auditd']
      }

      file { '/etc/audit/auditd.conf':
        ensure  => file,
        mode    => '0600',
        owner   => 'root',
        group   => 'root',
        content => template('shoretel_auditd/auditd.conf.erb'),
        notify  => Service['auditd'],
        require => [Package['audit'], Package['audit-libs']],
      }
      
      service { 'auditd':
        ensure    => true,
        enable    => true,
        path      => '/sbin/service',
        provider  => 'redhat'
      }
    }
  }
